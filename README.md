# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.

![](https://i.imgur.com/9ydZZ85.jpg)



## 工具欄
### Color picker
    點擊 color picker 方框，可以選擇更換筆刷、多邊形顏色

### Size slider
    拉動拉條，可以改變筆刷、橡皮擦、多邊形的大小粗細

### 筆刷
    點擊筆刷後，持續按住滑鼠可以在畫布上畫自由線條

### 橡皮擦
    點擊橡皮擦後，持續按住滑鼠可以在畫布上清除覆蓋區域

### 矩形
    點擊矩形後，持續按住滑鼠可以在畫布上拉出矩形(以滑鼠按下去的起始點為矩形一頂點)

### 三角形
    點擊三角形後，持續按住滑鼠可以在畫布上拉出等腰三角形，(以滑鼠按下去的起始點為三角形頂點)

### 圓形
    點擊圓形後，持續按住滑鼠可以在畫布上拉出圓形，(以滑鼠按下去的起始點為圓心)

### 文字輸入
    點擊Text後，可以在畫布中點擊任一點，輸入文字後按Enter即完成
    可以在下方選擇文字大小與字體

### Undo / Redo
    按Undo可以回復上一步驟，Redo可以直行下一步驟

### Refresh
    按Refresh可以清出畫布上所有東西

### Upload / Download
    按選擇檔案，可以選擇要上傳的圖片
    按Download可以下載當前畫布畫面

### Function description

    Decribe your bouns function and how to use it.

### Gitlab page link

https://108000207.gitlab.io/AS_01_WebCanvas

### Others (Optional)

[reference](
https://hackmd.io/T2C5AuKMRYuKo2wicHW7gA?view)

<style>
table th{
    width: 100%;
}
</style>