const myCanvas = document.getElementById('myCanvas');
const context = myCanvas.getContext('2d');
const myCanvas_2 = document.getElementById('myCanvas_2');   // using two canvas to deal with flickering
const context_2 = myCanvas_2.getContext('2d');

/// global variable for record the current state
let can_width = 1190;
let can_height = 680;

let states = {
    Brush: 0,
    Eraser: 1,
    Text: 2,
    Rectangle: 3,
    Triangle: 4,
    Circle: 5,
}
let cur_state = states.Brush;                   // initialize as brush
function change_state(new_state) {              // change the state and change cursor icon by changing the class
    console.log(new_state);
    cur_state = new_state;
    if (cur_state == states.Text) myCanvas.setAttribute("class", "text");
    else if (cur_state == states.Brush) myCanvas.setAttribute("class", "pen");
    else if (cur_state == states.Eraser) myCanvas.setAttribute("class", "eraser");
    else if (cur_state == states.Rectangle) myCanvas.setAttribute("class", "rectangle");
    else if (cur_state == states.Triangle) myCanvas.setAttribute("class", "triangle");
    else if (cur_state == states.Circle) myCanvas.setAttribute("class", "circle");
}


/// drawing pen
// When true, moving the mouse draws on the canvas
let isDrawing = false;
let isErasing = false;
let isRect = false;
let isCirc = false;
let isTri = false;
let x = 0;
let y = 0;

// event.offsetX, event.offsetY gives the (x,y) offset from the edge of the canvas.
// Add the event listeners for mousedown, mousemove, and mouseup
// syntax: target.addEventListener(type, listener [, options]);
myCanvas.addEventListener('mousedown', e => {               // when mouse down
    x = e.offsetX;
    y = e.offsetY;
    if (cur_state === states.Brush) {
        isDrawing = true;
    } else if (cur_state === states.Eraser) {
        isErasing = true;
    } else if (cur_state === states.Rectangle) {
        isRect = true;
        context_2.putImageData(cPushArray[cStep], 0, 0);    // draw the current image on the canvas_2
        context.clearRect(0, 0, can_width, can_height);     // let the original canvas be empty to draw rectangle, triangle, and circle
    } else if (cur_state === states.Circle) {
        isCirc = true;
        context_2.putImageData(cPushArray[cStep], 0, 0);
        context.clearRect(0, 0, can_width, can_height);
    } else if (cur_state === states.Triangle) {
        isTri = true;
        context_2.putImageData(cPushArray[cStep], 0, 0);
        context.clearRect(0, 0, can_width, can_height);
    }
});

myCanvas.addEventListener('mousemove', e => {               // when mouse moving, call draw function
    if (isDrawing === true) {
        drawLine(context, x, y, e.offsetX, e.offsetY);
        x = e.offsetX;
        y = e.offsetY;
    } else if (isErasing === true) {
        eraseLine(context, x, y, e.offsetX, e.offsetY);
        x = e.offsetX;
        y = e.offsetY;
    } else if (isRect === true) {
        drawRect(context, x, y, e.offsetX, e.offsetY, 0);
    } else if (isCirc === true) {
        drawCirc(context, x, y, e.offsetX, e.offsetY, 0);
    } else if (isTri === true) {
        drawTri(context, x, y, e.offsetX, e.offsetY, 0);
    }
});

myCanvas.addEventListener('mouseup', e => {                 // when mouse up
    if (isDrawing === true) {
        drawLine(context, x, y, e.offsetX, e.offsetY);      // last call draw
        x = 0;
        y = 0;
        isDrawing = false;                                  // reset as false
        cPush();                                            // push the current image to the stack
    } else if (isErasing === true) {
        eraseLine(context, x, y, e.offsetX, e.offsetY);
        x = 0;
        y = 0;
        isErasing = false;
        cPush();
    } else if (isRect === true) {                           // call draw with the context and context_2
        drawRect(context, x, y, e.offsetX, e.offsetY, context_2);   // to draw back the origial image on above canvas
        x = 0;
        y = 0;
        isRect = false;
        cPush();
    } else if (isCirc === true) {
        drawCirc(context, x, y, e.offsetX, e.offsetY, context_2);
        x = 0;
        y = 0;
        isCirc = false;
        cPush();
    } else if (isTri === true) {
        drawTri(context, x, y, e.offsetX, e.offsetY, context_2);
        x = 0;
        y = 0;
        isTri = false;
        cPush();
    }
});

function drawLine(context, x1, y1, x2, y2) {
    context.globalCompositeOperation="source-over";         // for writing
    context.beginPath();
    context.lineCap = 'round';
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
    context.closePath();
}
function eraseLine(context, x1, y1, x2, y2) {
    context.globalCompositeOperation = 'destination-out';   /// using transparent

    context.beginPath();
    context.lineCap = 'round';
    context.fill();
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
    context.closePath();
    context.globalCompositeOperation="source-over";         /// bug, or it will make every transport when the last step using eraser
}
function drawRect(context, x1, y1, x2, y2, up_flag) {
    context.globalCompositeOperation="source-over";         // for writing
    if (up_flag) {                                          // if mouse up(the last time call it)
        context.clearRect(0, 0, can_width, can_height);     // clear the above canvas
        context.putImageData(cPushArray[cStep], 0, 0);      // put back the origial image before rectangle
        up_flag.clearRect(0, 0, can_width, can_height);     // clear the canvas_2(below canvas)
        context.beginPath();                                // draw the final rectangle
        context.rect(x1, y1, x2 - x1, y2 - y1);
        context.stroke();
    } else {
        context.clearRect(0, 0, can_width, can_height);     // clear and draw the rectangle
        context.beginPath();
        context.rect(x1, y1, x2 - x1, y2 - y1);
        context.stroke();
    }
}
function drawCirc(context, x1, y1, x2, y2, up_flag) {       // draw a circle, and set the click spot as the center of the circle
    context.globalCompositeOperation="source-over";         // for writing
    if (up_flag) {
        context.clearRect(0, 0, can_width, can_height);
        context.putImageData(cPushArray[cStep], 0, 0);
        up_flag.clearRect(0, 0, can_width, can_height);

        context.beginPath();

        let radius = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        context.arc(x1, y1, radius, 0, 2 * Math.PI);
        context.stroke();
    } else {
        context.clearRect(0, 0, can_width, can_height);
        context.beginPath();
        let radius = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        context.arc(x1, y1, radius, 0, 2 * Math.PI);
        context.stroke();
    }
}
function drawTri(context, x1, y1, x2, y2, up_flag) {        // draw triangle, and set the click spot as the vertex of the triangle
    context.globalCompositeOperation="source-over";         // for writing
    if (up_flag) {
        context.lineCap = 'round';
        context.clearRect(0, 0, can_width, can_height);
        context.putImageData(cPushArray[cStep], 0, 0);
        up_flag.clearRect(0, 0, can_width, can_height);

        context.beginPath();
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        context.moveTo(x2, y2);
        context.lineTo(x2 - 2 * (x2 - x1), y2);
        context.moveTo(x2 - 2 * (x2 - x1), y2);
        context.lineTo(x1, y1);

        context.stroke();
    } else {
        context.lineCap = 'round';
        context.clearRect(0, 0, can_width, can_height);
        context.beginPath();
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        context.moveTo(x2, y2);
        context.lineTo(x2 - 2 * (x2 - x1), y2);
        context.moveTo(x2 - 2 * (x2 - x1), y2);
        context.lineTo(x1, y1);
        context.stroke();
    }
}



/// color picker

color_picker.addEventListener("change", changeColorPicker, false);
// colorPicker.addEventListener("input", updateFirst, false);
// only have to choose one of above

function changeColorPicker(event) {
    context.strokeStyle = event.target.value;
    context_2.strokeStyle = event.target.value;
}

/// pen width slider

var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
    context.lineWidth = slider.value;
    context_2.lineWidth = slider.value;
    output.innerHTML = this.value;
}

/// text

let cur_font = '20px Impact';
let hasInput = false;

myCanvas.onclick = function(e) {
    if (cur_state === states.Text) {
        if (hasInput) return;
        context.globalCompositeOperation="source-over";     // have to change, or the words will be transparent
        addText(e.clientX, e.clientY);
    }
}

//Function to dynamically add an input box: 
function addText(x, y) {

    let input = document.createElement('input');

    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - 4) + 'px';
    input.style.top = (y - 4) + 'px';

    input.onkeydown = handleEnter;
    document.body.appendChild(input);
    input.focus();
    hasInput = true;
}

// Key handler for input box:
function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
        cPush();
    }
}

// Draw the text onto canvas:
function drawText(txt, x, y) {
    context.textBaseline = 'top';
    context.textAlign = 'left';
    context.font = cur_font;
    context.fillText(txt, x - 10, y - 15);
}

/// text font and size
document.getElementById('text_size').onclick = function () {
    let new_size = document.getElementById("text_size").value;
    let new_font = document.getElementById("text_font").value;
    cur_font = new_size + new_font;
    cur_state = states.Text;
}

document.getElementById('text_font').onclick = function () {
    let new_size = document.getElementById("text_size").value;
    let new_font = document.getElementById("text_font").value;
    console.log(new_size);
    console.log(new_font);
    cur_font = new_size + new_font;
    console.log(cur_font);
    cur_state = states.Text;
}


/// Refresh
function refreshCanvas() {
    context.clearRect(0, 0, can_width, can_height);
    cPush();
}

/// Redo / Undo

let cPushArray = new Array();
let cStep = -1;
if (cStep === -1) {             // the first push into the stack, since if the first action is rect, it will not have the last record
    cPushArray.push(context.getImageData(0, 0, can_width, can_height));
    cStep++;
}

function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { 
        cPushArray.length = cStep; 
    }
    cPushArray.push(context.getImageData(0, 0, can_width, can_height)); // push the current photo to the array
}

function cUndo() {
    if (cStep > 1) {
        console.log(cStep);
        cStep--;
        context.putImageData(cPushArray[cStep], 0, 0);
    } else if (cStep == 1) {                                            // undo the first, so refresh the canvas immidiately
        context.clearRect(0, 0, can_width, can_height);
        cStep--;
    }
}

function cRedo() {
    if (cStep < cPushArray.length - 1) {
        console.log(cStep);
        cStep++;
        context.putImageData(cPushArray[cStep], 0, 0);
    }
}


/// upload
images.addEventListener('change', function(event) { // using the images(id) as the element(don't need to assign new var)
    let files = this.files;
    let container = this.parentNode;
    Array.prototype.forEach.call(files, file => {
        filePreview(file, container);
    })
})


function filePreview(file, container) {
    let url = URL.createObjectURL(file);
    let image = new Image();
    image.style.width = '600px';
    image.style.height = 'auto';
    image.alt = file.name;
    image.src = url;
    image.onload = function() {
        context.drawImage(image, 0, 0);         /// this is the bug that I suck
        cPush();
    };
  
}


/// download image
function download() {
    let download = document.getElementById("download");
    let image = document.getElementById("myCanvas").toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
}

